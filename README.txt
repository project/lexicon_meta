
Lexicon Meta
============


CONTENTS
--------

 * Introduction
 * Features
 * Requirements
 * Installation
 * Configuration
 * Roadmap
 * Similar Projects
 * Maintainers



INTRODUCTION
------------

Lexicon Meta was created to help <a href="http://www.lexiconofsustainability.com/" target="_blank">Lexicon of Sustainability</a> editors autopopulate meta content from content links, much like the functionality seen when posting links on Facebook, e.g., a user
pastes a link in a comment, and an image and a text blurb magically appear below.


FEATURES
--------

* Content is ajax loaded from OG Meta tags found in link placed in target field
* If OG Metas are absent, module looks for alternate content to suck into target fields.
* Media module is supported in addition to default image upload widget
* Youtube and vimeo video links are supported.



REQUIREMENTS
------------

* Dependency: <a href="http://drupal.org/project/filefield_sources" target="_blank">FileField Sources</a>
* If video support is required: <a href="https://www.drupal.org/project/media" target="_blank">Media</a>



INSTALLATION
------------

Install as you would normally install a contributed drupal module. See:
  https://drupal.org/documentation/install/modules-themes/modules-7
  for further information.



CONFIGURATION
-------------

 * The configuration for lexicon_meta is found via: admin/config/lexicon-meta-fields
 * Fields to target are defined via form at link above, i.e., fill these fields with the form id's (without '#')
 that should be populated from external url Metas.
 * The external url field (the source url for content pulling) should be a textfield.
 * For image or video assets, make sure to enable the external url file option associated with the field element (provided by
 FileField Sources module dependency).



ROADMAP
-------

* Enhanced Support for Media
* Standalone API



SIMILAR PROJECTS
----------------

* https://www.drupal.org/sandbox/karens/1988150



MAINTAINERS
-----------

* Ethan Teague (<a href="https://www.drupal.org/user/866306" target="_blank">EthanT<a>)
