<?php
/**
 * @file
 * This is the form include file.
 */

/**
 * Form constructor for the Lexicon Meta form.
 */
function lexicon_meta_form($form, &$form_state) {

  $form['lexicon_meta_url'] = array(
    '#type' => 'textfield',
    '#title' => t('The external url from which data is pulled.'),
    '#default_value' => variable_get('lexicon_meta_url'),
    '#description' => 'The input id of the url field.',
    '#required' => TRUE,
  );

  $form['lexicon_meta_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title field to populate'),
    '#default_value' => variable_get('lexicon_meta_title'),
    '#description' => 'The input id of the title field that will pull in the meta title',
    '#required' => TRUE,
  );

  $form['lexicon_meta_description'] = array(
    '#type' => 'textfield',
    '#title' => t('Main Text / Description Area'),
    '#default_value' => variable_get('lexicon_meta_description'),
    '#description' => 'The input id of the body field that will hold the Meta description',
  );

  $form['lexicon_meta_image'] = array(
    '#type' => 'fieldset',
    '#title' => t('Image Settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['lexicon_meta_image']['lexicon_meta_image_type'] = array(
    '#type' => 'select',
    '#title' => t('Selected'),
    '#options' => array(
      'standard' => t('Standard Upload'),
      'media_module' => t('Media Module Image'),
    ),
    '#default_value' => variable_get('lexicon_meta_image_type'),
    '#description' => t('Choose whether this is a standard upload or a Media module image.'),
  );

  $form['lexicon_meta_image']['lexicon_meta_image_path'] = array(
    '#type' => 'textfield',
    '#title' => t('Image field to populate'),
    '#default_value' => variable_get('lexicon_meta_image_path'),
    '#description' => 'The input id of the image field that will pull in the meta image.',
  );

  $form['lexicon_meta_video'] = array(
    '#type' => 'fieldset',
    '#title' => t('Youtube / Vimeo Media Settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#description' => 'Video Handling requires Media Module.',
  );

  if (module_exists('media')) {
    $form['lexicon_meta_video']['lexicon_meta_video_path'] = array(
      '#type' => 'textfield',
      '#title' => t('Video field to populate'),
      '#default_value' => variable_get('lexicon_meta_video_path'),
      '#description' => 'The input id of the video field that will pull in the video.',
    );
  }

  return system_settings_form($form);
}
