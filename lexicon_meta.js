(function($) {
  $(document).ready(function() {

    // We'll need this for our loading animation
    $('html').prepend('<div class="lexicon-meta-load"><div id="lexicon-spinner"></div></div>');

    // Pull in stuff when the link field detects keyup event
    $('#' + Drupal.settings.lexicon_meta.lexicon_meta_url).on('blur', function() {
      var content = $(this).val();
      var urlRegex = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
      var url = content.match(urlRegex);
      var form_token = $("[name='form_token']").val();
      var form_build_id = $("[name='form_build_id']").val();
      if (url.length > 0) {
	// Used to step thorough media module upload process
	function media_steps(click_field, first_click_time, modal_url, second_click_time) {
	  setTimeout(function(){
	    $('#' + click_field).click();
	  }, first_click_time);

	  setTimeout(function() {
	    var iframe = document.getElementById('mediaBrowser');
	    var iframeInner = iframe.contentDocument || iframe.contentWindow.document;
	    var modal_media_field = iframeInner.getElementById('edit-embed-code');
	    var modal_media_submit = iframeInner.querySelector('input[type="submit"]');
	    modal_media_field.value = modal_url;
	    modal_media_submit.click();
	  }, second_click_time);
	}

	// For cross domain stuff
	$.get(window.location.origin + '/og-page-pull?url=' + url + '&token=' + form_token + '&form_build_id=' + form_build_id, function(response) {
	  var html = response;
	  var el = document.createElement('html');
	  el.innerHTML = html;

	  // Build timers & show the background animation div
	  setTimeout(function() {
	    $('.lexicon-meta-load').css({
	      'display': 'block'
	    });
	  }, 0);
	  setTimeout(function() {

	    try {
	      // Populate the title
	      var title_source = el.querySelector('meta[property="og:title"]');
	      var title_source_fallback = el.querySelector('title');
	      var title_form = document.getElementById(Drupal.settings.lexicon_meta.lexicon_meta_title);

	      if (title_source) {
		title_form.value = title_source.content;
	      }
	      else if (title_source_fallback) {
		title_form.value = title_source_fallback.innerHTML;
	      }
	      else {
		title_form.value = 'Title is not available';
	      }
	    }
	    catch(err) {
	      err = 'The title is not available.';
	    }

	    try {
	      // Populate the body
	      var description_source = el.querySelector('meta[property="og:description"]');
	      var description_source_fallback = $('article > div p', el).contents().filter(function() {
		return $(this);
              });
	      var description_form = document.getElementById(Drupal.settings.lexicon_meta.lexicon_meta_description);
	      if (description_source) {
		description_form.value = description_source.content;
	      }
	      else if (description_source_fallback) {
		description_form.value = description_source_fallback.text();
	      }
	      else {
		description_form.value = 'Description is not available';
	      }
	    }
	    catch(err) {
	      err = 'The description is not available.';
	    }
	  }, 1500);

	  // Populate the video & click the video submit buttons
	  var str = $(url)[0];
	  var yt = str.search('youtube');
	  var vim = str.search('vimeo');
	  if (yt != -1 || vim != -1) {
	    try {
	      $('#edit-field-post-type-und').val('video').change();
	      media_steps(Drupal.settings.lexicon_meta.lexicon_meta_video_path, 100, url, 1200);

	      setTimeout(function() {
		$('.lexicon-meta-load').css({
		  'display': 'none'
		});
	      }, 4500);
	    }
	    catch(err) {
	      err = 'The video is not available.';
	    }
	  }
	  else {
	    try {
	      // Populate the image
	      var image_source = el.querySelector('meta[property="og:image"]');
	      var image_source_fallback_raw =  el.getElementsByTagName('img')[0];
	      var image_source_fallback = image_source_fallback_raw.getAttribute('src');
	      var image_form = document.getElementById(Drupal.settings.lexicon_meta.lexicon_meta_image_path);
	      if (image_source) {
		var asset_value = image_source.content;
	      }
	      else if (image_source_fallback) {
		var asset_value = image_source_fallback;
	      }
	      else {
		var asset_value = '';
	      }
	      image_form.value = asset_value;

	      if (Drupal.settings.lexicon_meta.lexicon_meta_image_type == 'standard') {

		// Click the image submit button
		if (image_form.value.length > 0) {
		  var e = jQuery.Event('keypress');
		  e.which = 13;
		  e.keyCode = 13;
		  $('#node-edit .image-widget .form-submit').trigger(e);
		}

	      }
	      else {
		media_steps(Drupal.settings.lexicon_meta.lexicon_meta_image_path, 100, asset_value, 1200);
	      }
	    }
	    catch(err) {
	      err = 'The image is not available.';
	    }
	    setTimeout(function() {
	      $('.lexicon-meta-load').css({
		'display': 'none'
	      });
	    }, 2500);
	  }
	});
      }
      return false;
    });
  });
}(jQuery));
